//Powered By zsCat, Since 2016 - 2020

package com.zs.pig.user.api.service;

import java.util.List;

import com.zs.pig.user.api.model.Member;

/**
* @author zsCat 2016-6-14 13:57:04
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的blog
 */

public interface MemberService{

	
	    int deleteByPrimaryKey(Long id);

	    int insertSelective(Member record);

	    Member selectByPrimaryKey(Long id);

	    int updateByPrimaryKeySelective(Member record);

	    List<Member> select(Member member);

		Member checkUser(String username, String password);
}
