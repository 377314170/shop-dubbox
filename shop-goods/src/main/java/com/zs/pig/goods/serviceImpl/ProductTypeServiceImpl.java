/** Powered By zscat科技, Since 2016 - 2020 */

package com.zs.pig.goods.serviceImpl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zs.pig.common.base.ServiceMybatis;
import com.zs.pig.goods.api.model.ProductType;
import com.zs.pig.goods.api.service.ProductTypeService;
import com.zs.pig.goods.mapper.ProductTypeMapper;
/**
 * 
 * @author zsCat 2016-12-22 9:29:55
 * @Email: [email]
 * @version [version]
 *	项目类别管理
 */
@Service("ProductTypeService")
public class ProductTypeServiceImpl  extends ServiceMybatis<ProductType> implements ProductTypeService {

	@Resource
	private ProductTypeMapper ProductTypeMapper;

	
	/**
	 * 保存或更新
	 * 
	 * @param ProductType
	 * @return
	 */
	public int saveProductType(ProductType ProductType) {
		return this.save(ProductType);
	}

	/**
	 * 删除
	* @param ProductType
	* @return
	 */
	public int deleteProductType(ProductType ProductType) {
		return this.delete(ProductType);
	}

   @Override
	public PageInfo<ProductType> findPageInfo(Map<String, Object> params) {
		PageHelper.startPage(params);
		List<ProductType> list = ProductTypeMapper.findPageInfo(params);
		return new PageInfo<ProductType>(list);
	}
}
