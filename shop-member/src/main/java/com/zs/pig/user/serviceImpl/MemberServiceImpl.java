//Powered By zsCat, Since 2014 - 2020

package com.zs.pig.user.serviceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zs.pig.common.sys.model.SysUser;
import com.zs.pig.common.utils.PasswordEncoder;
import com.zs.pig.user.api.model.Member;
import com.zs.pig.user.api.service.MemberService;
import com.zs.pig.user.mapper.MemberMapper;

/**
* @author zsCat 2016-6-14 13:57:04
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的blog
 */

@Service("MemberService")
public class MemberServiceImpl  implements MemberService {
    
	@Resource
	MemberMapper MemberMapper;
	@Override
	public int deleteByPrimaryKey(Long id) {
		// TODO Auto-generated method stub
		return MemberMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insertSelective(Member record) {
		// TODO Auto-generated method stub
		return MemberMapper.insertSelective(record);
	}

	@Override
	public Member selectByPrimaryKey(Long id) {
		// TODO Auto-generated method stub
		return MemberMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Member record) {
		// TODO Auto-generated method stub
		return MemberMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<Member> select(Member member) {
		// TODO Auto-generated method stub
		return MemberMapper.select(member);
	}

	@Override
	public Member checkUser(String username, String password) {
		Member sysUser = new Member();
		String secPwd = PasswordEncoder.encrypt(password, username);
		sysUser.setUsername(username);
		sysUser.setPassword(secPwd);
	//	sysUser.setDelFlag(Constant.DEL_FLAG_NORMAL);
		sysUser.setStatus(1);
		List<Member> users = MemberMapper.select(sysUser);
		if(users != null && users.size() == 1 && users.get(0) != null) {
			return users.get(0);
		}
		return null;
	}

	
	
}
