package com.zs.pig.shop.portl.beeltFunction;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.zs.pig.goods.api.model.Article;
import com.zs.pig.goods.api.model.Cart;
import com.zs.pig.goods.api.model.Product;
import com.zs.pig.goods.api.model.ProductClass;
import com.zs.pig.goods.api.service.ArticleService;
import com.zs.pig.goods.api.service.CartService;
import com.zs.pig.goods.api.service.ProductClassService;
import com.zs.pig.goods.api.service.ProductService;
import com.zs.pig.order.api.model.Order;
import com.zs.pig.order.api.service.OrderService;

@Component
public class GoodsFunction {

	
	@Resource
	private OrderService orderService;
	@Resource
	private ProductClassService ProductClassService;
	@Resource
	private CartService CartService;	
	@Resource
	private ProductService productService;
	@Resource
	private ArticleService articleService;
	
	public PageInfo<Product> getLatestGoods(int pageSize){
		return productService.selectPage(1, pageSize, new Product()," create_date desc");
	}
	public PageInfo<Product> getHitGoods(int pageSize){
		return productService.selectPage(1, pageSize, new Product()," clickHit desc");
	}
	public PageInfo<Product> getSellGoods(int pageSize){
		return productService.selectPage(1, pageSize, new Product()," sellhit desc");
	}
	
	/**
	 * 拿到推荐
	 * @param pageSize
	 * @return
	 */
	public PageInfo<Product> getCommendGoods(int pageSize){
		Product p=new Product();
		p.setIscom(1);
		return productService.selectPage(1, pageSize,p);
	}
	public PageInfo<Product> getTypeGoods(int pageSize,Long typeid){
		Product p=new Product();
		p.setTypeid(typeid);
		return productService.selectPage(1, pageSize,p);
	}
	public PageInfo<Product> getMenuTypeGoods(int pageSize,Long typeid){
		Product p=new Product();
		p.setType(typeid);
		return productService.selectPage(1, pageSize,p);
	}
	//活动最新订单
	public List<Order> getLatestOrder(int pageSize){
		return orderService.selectPage(1, pageSize, new Order());
	}
	//得到菜单类别
	public PageInfo<ProductClass> getProductClass(int pageSize,Long parentId){
		 ProductClass gc=new ProductClass();
         gc.setParentId(parentId);
		return ProductClassService.selectPage(1, pageSize, gc);
	}
	public List<ProductClass> getAllProductClass(){
		 ProductClass gc=new ProductClass();
		return ProductClassService.select(gc);
	}
	
	//得到购物车
	public List<Cart> getCartList() {
		 return null;
	 }
	
	//活动最新订单
		public PageInfo<Article> getAdvArticle(int pageSize){
			return articleService.selectPage(1, pageSize, new Article());
		}
}
