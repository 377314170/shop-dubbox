package com.zs.pig.shop.portl.beeltFunction;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.zs.pig.goods.api.model.Cart;
import com.zs.pig.goods.api.model.ProductClass;
import com.zs.pig.goods.api.service.CartService;
import com.zs.pig.goods.api.service.ProductClassService;
import com.zs.pig.order.api.model.Order;
import com.zs.pig.order.api.service.OrderService;


@Component
public class DgoodsFunction {

	
	@Resource
	private OrderService orderService;
	@Resource
	private ProductClassService ProductClassService;
	@Resource
	private CartService CartService;	
	
	

	//活动最新订单
	public List<Order> getLatestOrder(int pageSize){
		return orderService.selectPage(1, pageSize, new Order());
	}
	//得到菜单类别
	public PageInfo<ProductClass> getProductClass(int pageSize,Long parentId){
		 ProductClass gc=new ProductClass();
         gc.setParentId(parentId);
		return ProductClassService.selectPage(1, pageSize, gc);
	}
	
	
	//得到购物车
	public List<Cart> getCartList() {
		 return null;
		
	 }
}
